<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <style>
        body { 
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center; 
        background-size: 1366px 768px;
        }

        .navbar {
          background-color:#39ace7;
        }
    </style>
</head>
<body>
    <navbar class="navbar navbar-expand-md fixed-top" >
        <div class="container">
            <a class="navbar-brand" href="/home">   
                <img src="https://www.cattelecom.com/uploads/images/800px-CATTelecom_Logo.png" width="60" height="30">
            </a>
            <!-- <a class="navbar-brand" href="/">   
                {{ config('KhaoHorm', ' Toy crush ') }}
            </a> -->
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <div class="dropdown">
                    @guest
                    <li class="nav-item">
                        <a href="{{ route('loginPage') }}"><button type="button" class="btn btn-secondary"><font color="white">{{ ('เข้าระบบ') }}</font></button></a>
                    </li>
                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('signupPage') }}"><font color="white">{{ ('สมัครสมาชิก') }}</font></a>
                    </li>
                    @endif
                    @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                           <font color="white">{{ Auth::user()->username }} <span class="caret"></span></font>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">
                                {{ __('บัญชีผู้ใช้')}}
                            </a>
                            <a class="dropdown-item"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('ออกจากระบบ')}}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>

    </navbar>
    <br><br>
    <div class="container page">
        @yield('content')
    </div>
        @yield('scripts')
</body>
</html>
