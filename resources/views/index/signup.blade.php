<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>

  <body>
      <form action="{{ route('signup') }}" method="post">
      @csrf
          <div class="container text-center">
            <h1>Register</h1>
            <p>Please fill in this form to create an account.</p>
          </div>
          <div class="container" style="max-width: 400px;">
            <hr>
            <label for="username"><b>Username</b></label>
            <input type="text" name="username" id="username" required>
            <br>
            <label for="psw"><b>Password</b></label>
            <input type="password" name="password" autocomplete="off" id="password" required>
            <br>
            <label for="psw-repeat"><b>Repeat Password</b></label>
            <input type="password" name="password_confirmation" autocomplete="off" id="password_confirmation" required>
            <br>
            <label for="emp_prefix_name"><b>emp_prefix_name</b></label>
            <input type="text" name="emp_prefix_name" autocomplete="off" id="emp_prefix_name" required>
            <br>
            <label for="emp_firstname"><b>emp_firstname</b></label>
            <input type="text" name="emp_firstname" autocomplete="off" id="emp_firstname" required>
            <br>
            <label for="emp_lastname"><b>emp_lastname</b></label>
            <input type="text" name="emp_lastname" autocomplete="off" id="emp_lastname" required>
            <br>
            <label for="emp_department"><b>emp_department</b></label>
            <input type="text" name="emp_department" autocomplete="off" id="emp_department" required>
            <br>
            <label for="emp_part"><b>emp_part</b></label>
            <input type="text" name="emp_part" autocomplete="off" id="emp_part" required>
            <br>
            <label for="emp_position"><b>emp_position</b></label>
            <input type="text" name="emp_position" autocomplete="off" id="emp_position" required>
            <br>
            <label for="emp_Tel_internal"><b>emp_Tel_internal</b></label>
            <input type="text" name="emp_Tel_internal" autocomplete="off" id="emp_Tel_internal" required>
            <br>
            <label for="emp_Tel_personal"><b>emp_Tel_personal</b></label>
            <input type="text" name="emp_Tel_personal" autocomplete="off" id="emp_Tel_personal" required>
            <br>
            <label for="emp_email_cat"><b>emp_email_cat</b></label>
            <input type="text" name="emp_email_cat" autocomplete="off" id="emp_email_cat" required>
            <br>
            <label for="type"><b>type</b></label>
            <input type="text" name="type" autocomplete="off" id="type" required>
            <br>
            <label for="image_url"><b>image_url</b></label>
            <input type="text" name="image_url" autocomplete="off" id="image_url" required>

            <br><br>
            <button type="submit" class="registerbtn">Sign up</button>
        </div>

      </form>
  </body>
</html>
