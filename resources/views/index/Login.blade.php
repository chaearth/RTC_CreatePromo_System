<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
    <link rel="stylesheet" href="/css/LoginStyle.css">
  </head>

  <body>
    <div class="wrapper fadeInDown">
      <div id="formContent">
        <div class="card-header">Offering management system by &nbsp;<img src="https://www.cattelecom.com/uploads/images/800px-CATTelecom_Logo.png" width="60" height="30">
        </div>
        <br>
          @if (session('status'))
            <div class="alert alert-danger" role="alert">
                {{ session('status') }}
            </div>
          @endif
            <form id="login-form" class="form" action="{{ route('login') }}" method="post">
            @csrf
              <div class="form-group">
                <input type="username" class="form-control" name="username" id="username" placeholder="Enter username" autocomplete="no" required>
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="password" id="password" placeholder="Enter password" autocomplete="no" required>
              </div>
              <div class="form-group" align="center">
                <input type="submit" class="btn btn-info btn-md" value="Sign in">
              </div>
            </form>
      </div>
    </div>
  </body>
</html>