<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offersData extends Model
{
    protected $table = 'main_offering';

    /**
     * The attributes that are mass assignable.
     *

     * 
     * @var array
     */

    protected $fillable = ['offering_id', 'offering_name', 'offering_name_om', 'offering_name_en', 
        'offering_name_th', 'payment_mode', 'payment_mode_type', 'rental_fee_no_tax', 'rental_fee',
        'service_start_date', 'service_end_date', 'service_usage_end_date', 'account_code', 'revenue_code',
        'segment_code', 'product_code', 'ussd_apply', 'ussd_cancel', 'cycle_type', 'cycle_length', 'detail',
        'order_id', 'type_id'    
    ];
}
