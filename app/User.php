<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

const ADMIN_TYPE = 1;
const DEFAULT_TYPE = 0;

class User extends Authenticatable
{
    const ADMIN_TYPE = 1;
    const DEFAULT_TYPE = 0;

    protected $table = 'users';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 
        'emp_prefix_name', 'emp_firstname',
        'emp_lastname', 'emp_department', 'emp_part', 'emp_position',
        'emp_tel_internal', 'emp_tel_personal', 'emp_email_cat', 'type',
        'image_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->type === self::ADMIN_TYPE;
    }
}
