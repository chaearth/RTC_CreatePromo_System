<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\User\RegisterRequest;


class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.

     * @return array
     */
    public function rules()
    {
        return [
          'username' => 'required',
          'password' => 'required',
          // 'emp_prefix_name' => 'required',
          // 'emp_firstname' => 'required',
          // 'emp_lastname' => 'required',
          // 'emp_department' => 'required',
          // 'emp_part' => 'required',
          // 'emp_position' => 'required',
          // 'emp_tel_internal' => 'required',
          // 'emp_tel_personal' => 'required',
          // 'emp_email_cat' => 'required',
          // 'type' => 'required',
          // 'image_url' => 'required',
        ];
    }
}
