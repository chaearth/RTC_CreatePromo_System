<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'emp_prefix_name' => ['required', 'string', 'min:255', 'confirmed'],
            'emp_firstname' => ['required', 'string', 'min:255', 'confirmed'],
            'emp_lastname' => ['required', 'string', 'min:255', 'confirmed'],
            'emp_department' => ['required', 'string', 'min:255', 'confirmed'],
            'emp_part' => ['required', 'string', 'min:255', 'confirmed'],
            'emp_position' => ['required', 'string', 'min:255', 'confirmed'],
            'emp_tel_internal' => ['required', 'string', 'int:11', 'confirmed'],
            'emp_tel_personal' => ['required', 'string', 'int:11', 'confirmed'],
            'emp_email_cat' => ['required', 'string', 'min:255', 'confirmed'],
            'type' => ['required', 'string', 'min:255', 'confirmed'],
            'image_url' => ['required', 'string', 'min:255', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'emp_prefix_name' => $data['emp_prefix_name'],
            'emp_firstname' => $data['emp_firstname'],
            'emp_lastname' => $data['emp_lastname'],
            'emp_department' => $data['emp_department'],
            'emp_part' => $data['emp_part'],
            'emp_position' => $data['emp_position'],
            'emp_tel_internal' => $data['emp_tel_internal'],
            'emp_tel_personal' => $data['emp_tel_personal'],
            'emp_email_cat' => $data['emp_email_cat'],
            'type' => $data['type'],
            'image_url' => $data['image_url'],
        ]);
    }
}
