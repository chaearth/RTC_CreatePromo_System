<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\offersData;

class HomeController extends Controller
{
    public function HomePage()
    {
        $offers = offersData::all();

        return view('index.Home', ['offers' => $offers]);
    }
}
