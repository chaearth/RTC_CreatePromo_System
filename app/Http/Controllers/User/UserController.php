<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function signupPage(){
        return view('index.signup');
    }

    public function signup(RegisterRequest $request)
    {
        $userExist = User::where('username', $request->username)->exists();

        if ($userExist) {
            return redirect()->back()->with(['status' => 'user is already existing !!']);
        }

        $user = new user();

        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->emp_prefix_name = $request->emp_prefix_name;
        $user->emp_firstname = $request->emp_firstname;
        $user->emp_lastname = $request->emp_lastname;
        $user->emp_department = $request->emp_department;
        $user->emp_part = $request->emp_part;
        $user->emp_position = $request->emp_position;
        $user->emp_Tel_internal = $request->emp_Tel_internal;
        $user->emp_Tel_personal = $request->emp_Tel_personal;
        $user->emp_email_cat = $request->emp_email_cat;
        $user->type = $request->type;
        $user->image_url = $request->image_url;

        if (!$user->save()) {
            dd('fail');
        }

        return redirect('/');
    }

    public function login(Request $request)
    {
        $isAuth = Auth::attempt([
            'username' => $request->username,
            'password' => $request->password,
        ]);

        if (!$isAuth) {
            return redirect()->back()->with(['status' => 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง กรุณาลองอีกครั้ง !!']);
        }
        if (Auth::check() && Auth::user()->isAdmin()) {
            return redirect()->route('dashboard.page')->with('alert', 'เข้าสู่ระบบสำเร็จAdmin !!');
        }

        return redirect('/home')->with('alert', 'เข้าสู่ระบบสำเร็จ User !!');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
