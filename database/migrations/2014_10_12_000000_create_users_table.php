<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('password');
            $table->string('emp_prefix_name');
            $table->string('emp_firstname');
            $table->string('emp_lastname');
            $table->string('emp_department');
            $table->string('emp_part');
            $table->string('emp_position');
            $table->integer('emp_Tel_internal');
            $table->integer('emp_Tel_personal');
            $table->string('emp_email_cat');
            $table->string('type');
            $table->string('image_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
