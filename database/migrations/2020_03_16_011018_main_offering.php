<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MainOffering extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_offering', function (Blueprint $table) {
            $table->bigIncrements('offering_id');
            $table->string('offering_name');
            $table->string('offering_name_om');
            $table->string('offering_name_en');
            $table->string('offering_name_th');
            $table->string('payment_mode');
            $table->string('payment_mode_type');
            $table->string('rental_fee_no_tax');
            $table->string('rental_fee');
            $table->date('service_start_date');
            $table->date('service_end_date');
            $table->date('service_usage_end_date');
            $table->string('account_code');
            $table->string('revenue_code');
            $table->string('segment_code');
            $table->string('product_code');
            $table->string('ussd_apply');
            $table->string('ussd_cancel');
            $table->string('cycle_type');
            $table->string('cycle_length');
            $table->string('detail');
            $table->string('order_id');
            $table->string('type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
